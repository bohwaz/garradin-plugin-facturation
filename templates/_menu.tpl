<style>
{include file="%s/templates/_style.css"|args:$plugin_root}
</style>
<nav class="tabs">
<ul>
    <li{if $current == 'index'} class="current"{/if}><a href="{$plugin_admin_url}">Liste documents</a></li>
    {if $session->canAccess($session::SECTION_ACCOUNTING, $session::ACCESS_WRITE)}
    <li{if $current == 'facture'} class="current"{/if}><a href="{$plugin_admin_url}facture_ajouter.php">Nouveau document</a></li>
    {/if}
    <li{if $current == 'clients'} class="current"{/if}><a href="{$plugin_admin_url}clients.php">Liste clients</a></li>
    {if $session->canAccess($session::SECTION_ACCOUNTING, $session::ACCESS_ADMIN)}
    <li{if $current == 'config'} class="current"{/if}><a href="{$plugin_admin_url}config.php">Configuration</a></li>
    {/if}
    <li{if $current == 'aide'} class="current"{/if}><a href="{$plugin_admin_url}aide.php">Aide</a></li>
</ul>
</nav>