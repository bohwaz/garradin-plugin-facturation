<?php

namespace Paheko;

require_once __DIR__ . '/_inc.php';

if (!isset($target) || !in_array( $target, ['new', 'edit'])) {
	throw new Exception('blabla illegal call'); // Fix: exception type?
} else {
	$target = ($target === 'new' ? true:false);
}

$session->requireAccess($session::SECTION_ACCOUNTING, $session::ACCESS_WRITE);

use Paheko\DB;
use stdClass;
use const \Paheko\Plugin\Facturation\PATTERNS_LIST;

$db = DB::getInstance();

$step = false;
$radio = $liste = $designations = $prix = [];

$fields = $facture->recu_fields;

$moyens_paiement = $facture->listMoyensPaiement(true);

$tpl->assign('moyens_paiement', $moyens_paiement);
$tpl->assign('moyen_paiement', f('moyen_paiement') ?: 'ES');
$tpl->assign('moyen_paiement_cerfa', f('moyen_paiement_cerfa') ?: 'ES');

$tpl->assign('formes_don', array('1' => 'Acte authentique',
                                 '2' => 'Acte sous seing privé',
                                 '3' => 'Don manuel',
                                 '4' => 'Autres'));
$tpl->assign('natures_don', array('1' => 'Numéraire',
                                 '2' => 'Chèque',
                                 '3' => 'Virement, CB; ...'));
$tpl->assign('textes_don', $facture->listTextesCerfa());

if ( !$target ) {
	f(['id' => 'required|numeric']);
	$id = (int) qg('id');
	
	if (!$f = $facture->get($id))
	{
		throw new UserException("Ce document n'existe pas.");
	}
}

// Traitement

if (f('save'))
{
	$str_moyen = 'moyen_paiement'. (f('type') == CERFA ? '_cerfa':'');

    $form->check($csrf_key, [
        'type' => 'required|in:'.implode(',', [DEVIS, FACT, CERFA]),
        'numero_facture' => $require_number ? 'required|string' : 'string',
        'date_emission' => 'required|date_format:d/m/Y',
        'date_echeance' => 'required|date_format:d/m/Y',
        // 'reglee' => '',
        // 'archivee' => '',
        'base_receveur' => 'required|in:membre,client',
        // 'client' => '',
        // 'membre' => '',
        $str_moyen => 'required|in:' . implode(',', array_keys($moyens_paiement)),
        'designation' => 'array|required',
        'prix' => 'array|required'
    ]);

    if (!$form->hasErrors())
    {
        try
        {
            if ( count(f('designation')) !== count(f('prix')) )
            {
                throw new UserException('Nombre de désignations et de prix reçus différent.');
            }

            $data = [
                'numero'			=> f('numero_facture'),
                'date_emission'		=> f('date_emission'),
                'date_echeance'		=> f('date_echeance'),
                'reglee'			=> f('reglee') == 1?1:0,
                'archivee'			=> f('archivee') == 1?1:0,
                'moyen_paiement'	=> f('moyen_paiement'),
                'toto'				=> 0
            ];
            $data['type_facture'] = f('type');
            if (in_array(f('type'), [DEVIS, FACT]))
            {
                foreach(f('designation') as $k=>$value)
                {
                    $data['contenu'][$k]['designation'] = $value;
                    $data['contenu'][$k]['prix'] = Utils::moneyToInteger(f('prix')[$k]);
                    $data['toto'] += Utils::moneyToInteger(f('prix')[$k]);
                }
                $data['total'] = $data['toto'];
                unset($data['toto']);
            }
            elseif ( f('type') == CERFA )
            {
                $data['moyen_paiement'] = f('moyen_paiement_cerfa');
                $data['contenu'] = [
                    'forme' => f('forme_don'),
                    'nature' => f('nature_don'),
                    'texte' => f('texte_don')];
				$data['total'] = Utils::moneyToInteger(f('total'));
				unset($data['toto']);
            }
            if (f('base_receveur') == 'client')
            {
                $data['receveur_membre'] = 0;
                $data['receveur_id'] = f('client');
            }
            elseif (f('base_receveur') == 'membre')
            {
                $data['receveur_membre'] = 1;
                $data['receveur_id'] = f('membre');
            }


        }
        catch(UserException $e)
        {
            $form->addError($e->getMessage());
        }
    }
}
elseif (f('select_cotis'))
{
    $form->check('add_cotis_1',[
		'numero_facture' => $require_number ? 'required|string' : 'string',
        'date_emission' => 'required|date_format:d/m/Y',
        'membre_cotis' => 'required|numeric',
    ]);

    $step = true;
}
elseif (f('add_cotis'))
{
    $form->check('add_cotis_2',[
		'numero_facture' => $require_number ? 'required|string' : 'string',
        'date_emission' => 'required|date_format:d/m/Y',
        'membre_cotis' => 'required|numeric',
        'cotisation' => 'required',
        ]);

    $radio['type'] = f('cotisation');
    
    if (!$form->hasErrors())
    {
        try
        {
            $num = (int) str_replace('cotis_', '', $radio['type']);
            foreach($fields as $field)
            {
                $cotis[$field] = f($field.'_'.$num);
            }

            $r = $facture->getCotis(f('membre_cotis'), $cotis['id']);
            $r = $r[0];

            $data = [
                'type_facture' => COTIS,
                'numero' => f('numero_facture'),
                'receveur_membre' => 1,
                'receveur_id' => f('membre_cotis'),
                'date_emission' => f('date_emission'),
				'moyen_paiement'	=> 'AU',
                'total' => $r->paid_amount ?? $r->amount,
                'contenu' => ['id' => $cotis['id'],
                            'intitule' => $cotis['label'],
                            'souscription' => $cotis['date'],
                            'expiration' => $cotis['expiry'] ]
            ];

        }
        catch (UserException $e)
        {
            $form->addError($e->getMessage());
        }
    }
}

if ($step)
{
    try
    {
        $liste = $facture->getCotis((int)f('membre_cotis'));
    }
    catch (UserException $e)
    {
        $form->addError($e->getMessage());
    }
}
elseif (isset($data))
{
	if ($target)
	{
		$id = $facture->add($data, $plugin->getConfig('pattern'));
		Utils::redirect(PLUGIN_ADMIN_URL  . 'facture.php?id='.(int)$id);
	}
	else
	{
		if ($facture->edit($id, $data))
		{
			Utils::redirect(PLUGIN_ADMIN_URL  . 'facture.php?id='.(int)$id);
		}
		throw new UserException('Erreur d\'édition du reçu');
	}
}

// Affichage

if ($target)
{
	$doc = null;

	if (qg('copy') !== null && $f = $facture->get((int)qg('copy'))) {
		$doc = (array) $f;
		// Copié depuis facture_modifier.php
		$doc['type']           = $f->type_facture;
		$doc['numero_facture'] = '';
		$doc['base_receveur']  = $f->receveur_membre ? 'membre' : 'client';
		$doc['client']         = $f->receveur_id;
		$doc['membre']         = $f->receveur_id;

		if ( $f->type_facture == CERFA ) {
			$doc['forme_don'] = $f->contenu['forme'];
			$doc['nature_don'] = $f->contenu['nature'];
			$doc['texte_don'] = $f->contenu['texte'];
		}
	}

	// Type du document:
	$type = qg('t') ? (int) qg('t') : null;

	// Si le type est défini dans l'URL
	if (in_array($type, [DEVIS, FACT, CERFA, COTIS], true))
	{
		$radio['type'] = $type;
	} // ... s'il a été rempli dans le formulaire envoyé
	elseif (null !== f('type'))
	{
		$radio['type'] = f('type');
	} // ... s'il est défini dans le document copié
	elseif (isset($doc['type'])) {
		$radio['type'] = $doc['type'];
	} // ... ou par défaut
	else 
	{
		$radio['type'] = FACT;
	}
}
else 
{
	$doc['moyen_paiement']	= $f->moyen_paiement;
	$doc['type'] 			= $f->type_facture;
	$doc['numero_facture']	= $f->numero;
	$doc['reglee']			= $f->reglee;
	$doc['base_receveur']	= $f->receveur_membre?'membre':'client';
	$doc['client']			= $f->receveur_id;
	$doc['membre']			= $f->receveur_id;
	$doc['contenu']			= $f->contenu;
	
	$doc['date_emission'] = f('date_emission') ?: $f->date_emission;
	$doc['date_echeance'] = f('date_echeance')?: $f->date_echeance; // Smarty m'a saoulé pour utiliser form_field|date_fr:---
	/* modif DD -- CERFA -------------------------------------- */
	if ( $f->type_facture == CERFA ) {
		$doc['total'] = $f->total;
		$doc['forme_don'] = $f->contenu['forme'];
		$doc['nature_don'] = $f->contenu['nature'];
		$doc['texte_don'] = $f->contenu['texte'];
	}
	
	$radio['type'] = f('type')??$doc['type'];
}
$tpl->assign('types_details', $facture->types);

$tpl->assign('client_id', f('client') ?: -1);
$tpl->assign('membre_id', f('membre') ?: -1);


// C'est un peu l'équivalent de form_field, mais j'avais écrit ça avant
// et oulala, c'est un peu complexe, faudrait réfléchir keskivomieux
$from_user = false;
if (in_array($radio['type'], [DEVIS, FACT]))
{
	if (($d = f('designation')) && ($p = f('prix')) && implode($d))
	{
		foreach($d as $k=>$v)
		{
			if (empty($v) && empty($p[$k]))
			{
				continue;
			}
			$designations[] = $v;
			$prix[] = $p[$k];
		}
		$from_user = true;
	}
	else if (!empty($doc['contenu'])) {
		foreach($doc['contenu'] as $k=>$v)
		{
			if (empty($v['designation']) && empty($v['prix']))
			{
				continue;
			}
			$designations[] = $v['designation'];
			$prix[] = $v['prix'];
		}
	}
	else {
		$designations = ['Exemple'];
		$prix = [250];
	}
}

$date = new \DateTime;
$date->setTimestamp(time());
$tpl->assign('date', $date->format('d/m/Y'));


$tpl->assign(compact('liste', 'radio', 'step', 'designations', 'prix', 'from_user', 'identite', 'csrf_key', 'doc'));
$tpl->assign('users', $db->getAssoc('SELECT id, '.$identite.' FROM users WHERE id_category != -2 NOT IN (SELECT id FROM users_categories WHERE hidden = 1);'));
$tpl->assign('clients', $db->getAssoc('SELECT id, nom FROM plugin_facturation_clients;'));
$tpl->assign('require_number', $require_number);
$tpl->assign('number_pattern', PATTERNS_LIST[$plugin->getConfig('pattern')]);

if ($target) {
	$tpl->display(PLUGIN_ROOT . '/templates/facture_ajouter.tpl');
} else {
	$tpl->display(PLUGIN_ROOT . '/templates/facture_modifier.tpl');
}