<?php

namespace Paheko;

require_once __DIR__ . '/_inc.php';

$session->requireAccess($session::SECTION_ACCOUNTING, $session::ACCESS_READ);

use Paheko\Users\Users;

$users = new Users;

f(['id' => 'required|numeric']);
$id = (int) qg('id');

$f = $facture->get($id);

if (!$f)
{
	throw new UserException("Ce document n'existe pas.");
}

$tpl->assign('type', $f->type_facture);
$tpl->assign('facture', $f);     
$tpl->assign('id', $id);
$tpl->assign('footer', $plugin->getConfig('footer')?:'');
$tpl->assign('siret_asso', $plugin->getConfig('siret_asso')?:'');

$tpl->display(PLUGIN_ROOT . '/templates/facture.tpl');
