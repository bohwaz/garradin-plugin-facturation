<?php

namespace Paheko;

require_once __DIR__ . '/_inc.php';

$session->requireAccess($session::SECTION_ACCOUNTING, $session::ACCESS_ADMIN);

$form->runIf('save', function () use ($plugin) {
	$plugin->setConfigProperty('rna_asso', trim(f('rna_asso')));
	$plugin->setConfigProperty('siret_asso', trim(f('siret_asso')));
	$plugin->setConfigProperty('ttc', (bool) f('ttc'));

	$plugin->setConfigProperty('numero_rue_asso', trim(f('numero_rue_asso')));
	$plugin->setConfigProperty('rue_asso', trim(f('rue_asso')));
	$plugin->setConfigProperty('cp_asso', trim(f('cp_asso')));
	$plugin->setConfigProperty('ville_asso', trim(f('ville_asso')));

	$plugin->setConfigProperty('droit_art200', (bool)f('droit_art200'));
	$plugin->setConfigProperty('droit_art238bis', (bool)f('droit_art238bis'));
	$plugin->setConfigProperty('droit_art885_0VbisA', (bool)f('droit_art885_0VbisA'));
	$plugin->setConfigProperty('objet_0', trim(f('objet_0')));
	$plugin->setConfigProperty('objet_1', trim(f('objet_1')));
	$plugin->setConfigProperty('objet_2', trim(f('objet_2')));;

	$plugin->setConfigProperty('footer', f('footer'));
	
	$plugin->setConfigProperty('validate_cp', (bool)f('validate_cp'));
	$plugin->setConfigProperty('unique_client_name', (bool)f('unique_client_name'));

	$plugin->setConfigProperty('pattern', f('pattern'));

	$plugin->save();
}, 'facturation_config', PLUGIN_ADMIN_URL . 'config.php?ok');


$tpl->assign('ok', qg('ok') !== null);
$tpl->assign('conf', $plugin->getConfig());
$tpl->assign('patterns', \Paheko\Plugin\Facturation\PATTERNS_LIST);

$tpl->display(PLUGIN_ROOT . '/templates/config.tpl');